package com.chatchat.service;

import com.chatchat.common.code.SignUpStatus;
import com.chatchat.entity.User;
import com.chatchat.exception.DuplicatedException;
import com.chatchat.exception.ErrorCode;
import com.chatchat.exception.NotFoundException;
import com.chatchat.exception.SignUpExpirationException;
import com.chatchat.model.CompleteSignUpResponse;
import com.chatchat.model.InCompleteSignUpRequest;
import com.chatchat.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import java.time.ZonedDateTime;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.mock;

public class SignUpServiceTest {

    private final UserRepository userRepository = mock(UserRepository.class);
    private final SignUpService signUpService = new SignUpServiceImpl(userRepository);

    @DisplayName("회원가입 완료")
    @ParameterizedTest
    @CsvSource({"foo@test.com,fooPwd,foo", "jane@test.com,janePwd,jane"})
    void inCompleteSignUp(String email, String userPwd, String userName) {
        //given
        given(userRepository.save(any())).willReturn(inCompleteUser(email, userPwd, userName, ZonedDateTime.now().plusDays(1)));
        //when
        var request = new InCompleteSignUpRequest(email, userPwd, userName);
        var response = signUpService.inCompleteSignUp(request);
        //then
        Assertions.assertEquals(userName, response.getUserName());
        Assertions.assertEquals(email, response.getEmail());
    }

    @DisplayName("이메일 중복")
    @Test
    void inCompleteSignUpDuplicatedEmail() {
        //given
        given(userRepository.findByEmail("foo@test.com"))
                .willReturn(Optional.of(inCompleteUser("foo@test.com", "fooPwd", "foo", ZonedDateTime.now().plusDays(1))));
        //when
        var request = new InCompleteSignUpRequest("foo@test.com", "fooPwd2", "foo2");
        DuplicatedException exception = Assertions.assertThrows(DuplicatedException.class, () -> signUpService.inCompleteSignUp(request));
        //then
        Assertions.assertEquals(ErrorCode.DUPLICATED_EMAIL, exception.getErrorCode());
        Assertions.assertEquals(ErrorCode.DUPLICATED_EMAIL.getMessage(), exception.getMessage());
    }

    private User inCompleteUser(String email, String password, String userName, ZonedDateTime signUpExpiredAt) {
        User user = new User(email, password, userName);
        user.setSignUpStatus(SignUpStatus.IN_COMPLETE);
        user.setToken(generateToken(email));
        user.setSignUpExpiredAt(signUpExpiredAt);
        return user;
    }

    private String generateToken(String email) {
        return "TOKEN-" + email;
    }

    @DisplayName("회원가입 이메일 검증 완료")
    @Test
    void completeSignUp() {
        //given
        given(userRepository.findByToken("TOKEN-foo@test.com")).willReturn(
                Optional.of(inCompleteUser("foo@test.com", "fooPwd", "foo", ZonedDateTime.now().plusDays(1))));
        given(userRepository.expiredSignUpToken("TOKEN-foo@test.com")).willReturn(false);
        given(userRepository.updateByCompleteSignUpStatus("foo@test.com"))
                .willReturn(completeUser("foo@test.com", "fooPwd", "foo", ZonedDateTime.now().plusDays(1)));
        //when
        CompleteSignUpResponse response = signUpService.completeSignUp("TOKEN-" + "foo@test.com");
        //then
        Assertions.assertEquals("foo@test.com", response.getEmail());
        Assertions.assertEquals("foo", response.getUserName());
    }

    private User completeUser(String email, String password, String userName, ZonedDateTime signUpExpiredAt) {
        User user = new User(email, password, userName);
        user.setSignUpStatus(SignUpStatus.IN_COMPLETE);
        user.setToken("TOKEN-" + email);
        user.setSignUpExpiredAt(signUpExpiredAt);
        return user;
    }

    @DisplayName("회원 가입 토큰 존재 안함")
    @Test
    void completeSignUpNotExistSignUpToken() {
        //given
        given(userRepository.findByToken("TOKEN-foo@test.com")).willReturn(
                Optional.of(inCompleteUser("foo@test.com", "fooPwd", "foo", ZonedDateTime.now().plusDays(1))));
        //when
        NotFoundException exception = Assertions.assertThrows(NotFoundException.class, () -> signUpService.completeSignUp("TOKEN-jane@test.com"));
        //then
        Assertions.assertEquals(ErrorCode.NOT_FOUND, exception.getErrorCode());
        Assertions.assertEquals(ErrorCode.NOT_FOUND.getMessage(), exception.getMessage());
    }

    @DisplayName("회원 가입 이메일 검증 기간 만료")
    @Test
    void completeSignUpExpiredDateTime() {
        //given
        given(userRepository.findByToken("TOKEN-foo@test.com")).willReturn(
                Optional.of(inCompleteUser("foo@test.com", "fooPwd", "foo", ZonedDateTime.now())));
        given(userRepository.expiredSignUpToken("TOKEN-foo@test.com")).willReturn(true);
        //when
        SignUpExpirationException exception = Assertions.assertThrows(SignUpExpirationException.class,
                () -> signUpService.completeSignUp("TOKEN-foo@test.com"));
        //then
        Assertions.assertEquals(ErrorCode.SIGNUP_EXPIRATION, exception.getErrorCode());
        Assertions.assertEquals(ErrorCode.SIGNUP_EXPIRATION.getMessage(), exception.getMessage());
    }


}
