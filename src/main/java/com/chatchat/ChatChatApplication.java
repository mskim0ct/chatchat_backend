package com.chatchat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

//@EnableJpaRepositories("com.chatchat.repository")
@SpringBootApplication
public class ChatChatApplication {

    public static void main(String[] args){
        SpringApplication.run(ChatChatApplication.class, args);
    }
}
