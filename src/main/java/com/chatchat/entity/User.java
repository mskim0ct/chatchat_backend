package com.chatchat.entity;

import com.chatchat.common.code.SignUpStatus;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Entity
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;
    @Column
    private String email;
    @Column
    private String password;
    @Column
    private String userName;
    @Column
    private String token;
    @Column
    private ZonedDateTime createdAt;
    @Column
    private ZonedDateTime modifiedAt;
    @Column
    private SignUpStatus signUpStatus;
    @Column
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private ZonedDateTime signUpExpiredAt;

    public User(){
    }

    public User(String email, String password, String userName){
        this.email = email;
        this.password = password;
        this.userName = userName;
    }

    public Long getId(){
        return id;
    }
    public String getUserName(){
        return userName;
    }
    public String getEmail(){
        return email;
    }
    public String getToken(){
        return token;
    }
    public ZonedDateTime getCreatedAt(){
        return createdAt;
    }
    public ZonedDateTime getModifiedAt(){
        return modifiedAt;
    }
    public SignUpStatus getSignUpStatus(){
        return signUpStatus;
    }
    public ZonedDateTime getSignUpExpiredAt(){
        return signUpExpiredAt;
    }
    public void setUserName(String userName){
        this.userName = userName;
    }
    public void setToken(String token){
        this.token = token;
    }
    public void setSignUpStatus(SignUpStatus signUpStatus) {
        this.signUpStatus = signUpStatus;
    }
    public void setSignUpExpiredAt(ZonedDateTime signUpExpiredAt){
        this.signUpExpiredAt = signUpExpiredAt;
    }

    @PrePersist
    public void prePersist(){
        this.token = "TOKEN-"+email;
        this.signUpStatus = SignUpStatus.IN_COMPLETE;

        ZonedDateTime currentDateTime = ZonedDateTime.now();
        this.createdAt = currentDateTime;
        this.modifiedAt = currentDateTime;
        this.signUpExpiredAt = currentDateTime.plusDays(1);
    }

    @PreUpdate
    public void preUpdate(){
        this.modifiedAt = ZonedDateTime.now();
    }
}
