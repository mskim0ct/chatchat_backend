package com.chatchat.common.code;

public enum SignUpStatus {
    IN_COMPLETE, COMPLETED
}
