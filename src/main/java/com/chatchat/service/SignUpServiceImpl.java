package com.chatchat.service;

import com.chatchat.entity.User;
import com.chatchat.exception.DuplicatedException;
import com.chatchat.exception.ErrorCode;
import com.chatchat.exception.NotFoundException;
import com.chatchat.exception.SignUpExpirationException;
import com.chatchat.model.CompleteSignUpResponse;
import com.chatchat.model.InCompleteSignUpRequest;
import com.chatchat.model.InCompleteSignUpResponse;
import com.chatchat.repository.UserRepository;

public class SignUpServiceImpl implements SignUpService {

    private final UserRepository userRepository;

    public SignUpServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public InCompleteSignUpResponse inCompleteSignUp(InCompleteSignUpRequest request) {
        checkEmail(request.getEmail());
        User user = saveUser(request);
        return new InCompleteSignUpResponse(user.getEmail(), user.getUserName());
    }

    private void checkEmail(String email) {
        userRepository.findByEmail(email)
                .ifPresent(notUsed -> {
                    throw new DuplicatedException(ErrorCode.DUPLICATED_EMAIL,
                            ErrorCode.DUPLICATED_EMAIL.getMessage());
                });
    }

    private User saveUser(InCompleteSignUpRequest request) {
        User user = new User(request.getEmail(), request.getPassword(), request.getUserName());
        return userRepository.save(user);
    }

    @Override
    public CompleteSignUpResponse completeSignUp(String signUpToken) {
        User user = validSignUpToken(signUpToken);
        User updatedUser = updateUserForCompleteSignUp(user.getEmail());
        return new CompleteSignUpResponse(updatedUser.getEmail(), updatedUser.getUserName());
    }

    private User updateUserForCompleteSignUp(String email) {
        return userRepository.updateByCompleteSignUpStatus(email);
    }

    private User validSignUpToken(String signUpToken) {
        User user = userRepository.findByToken(signUpToken)
                .orElseThrow(
                        () -> {
                            throw new NotFoundException(ErrorCode.NOT_FOUND,
                                    ErrorCode.NOT_FOUND.getMessage()
                            );
                        }
                );
        if (userRepository.expiredSignUpToken(signUpToken)) {
            throw new SignUpExpirationException(ErrorCode.SIGNUP_EXPIRATION,
                    ErrorCode.SIGNUP_EXPIRATION.getMessage());
        }
        return user;
    }

}
