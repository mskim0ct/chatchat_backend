package com.chatchat.service;

import com.chatchat.model.CompleteSignUpResponse;
import com.chatchat.model.InCompleteSignUpRequest;
import com.chatchat.model.InCompleteSignUpResponse;

public interface SignUpService {
    InCompleteSignUpResponse inCompleteSignUp(InCompleteSignUpRequest request);
    CompleteSignUpResponse completeSignUp(String signUpToken);

}
