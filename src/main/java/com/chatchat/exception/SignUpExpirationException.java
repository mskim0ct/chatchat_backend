package com.chatchat.exception;

public class SignUpExpirationException extends RuntimeException{
    private ErrorCode errorCode;
    private String message;

    public SignUpExpirationException(ErrorCode errorCode, String message){
        this.errorCode = errorCode;
        this.message = message;
    }

    public ErrorCode getErrorCode(){
        return errorCode;
    }
    public String getMessage(){
        return message;
    }
}
