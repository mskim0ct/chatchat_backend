package com.chatchat.exception;

public enum ErrorCode {
    DUPLICATED_USER_ID("해당 아이디는 이미 존재합니다."),
    DUPLICATED_EMAIL("해당 이메일은 이미 존재합니다."),
    SIGNUP_EXPIRATION("회원가입 기한이 만료되었습니다."),
    NOT_FOUND("찾을 수 없습니다.");

    private String message;

    ErrorCode(String message){
        this.message = message;
    }
    public String getMessage(){
        return message;
    }

}
