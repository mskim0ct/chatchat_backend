package com.chatchat.repository;

import com.chatchat.entity.User;

import java.util.Optional;

public interface UserRepository<T extends User> {
    Optional<T> findByUserId(String userId);
    Optional<T> findByEmail(String email);
    Optional<T> findByToken(String signUpToken);
    boolean expiredSignUpToken(String signUpToken);
    T save(T t);
}
