package com.chatchat.model;

public class InCompleteSignUpRequest {
    private String email;
    private String password;
    private String userName;

    public InCompleteSignUpRequest(String email, String password, String userName) {
        this.email = email;
        this.password = password;
        this.userName = userName;
    }

    public String getUserName(){
        return userName;
    }
    public String getPassword(){
        return password;
    }
    public String getEmail(){
        return email;
    }
}
