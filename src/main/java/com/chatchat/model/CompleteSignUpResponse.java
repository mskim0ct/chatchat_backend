package com.chatchat.model;

public class CompleteSignUpResponse {
    private String userName;
    private String email;

    public CompleteSignUpResponse(String email, String userName){
        this.userName = userName;
        this.email = email;
    }
    public String getUserName(){
        return userName;
    }
    public String getEmail(){
        return email;
    }
}
