package com.chatchat.model;

public class InCompleteSignUpResponse {
    private String email;
    private String userName;

    public InCompleteSignUpResponse(String email, String userName){
        this.email = email;
        this.userName = userName;
    }
    public String getEmail(){
        return email;
    }
    public String getUserName(){
        return userName;
    }
}
